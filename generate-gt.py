import pandas as pd
import os, sys
import time
import numpy as np

# Loading original dataset
DATASET = 'fakeWhatsApp.BR_2020_partial.csv'
df = pd.read_csv(os.path.join('data', '2020', DATASET))
df = df[df['misinformation'].notnull()]

# Loading already saved random indexes from original df
indexes_df = pd.read_csv('indexes.csv')
random_indexes = indexes_df['original_index'].to_list()
# random_indexes = random_indexes[:5] # TODO: Remove this

MAX_QUESTIONS = len(random_indexes) ** 2
REAL_FILLS_MAX = 4950
# get_num_marks() = os.get_terminal_size(0)[0]

# Selecting only desired random indexes from original df
# df_ = df.loc[df.index[random_indexes]]

def create_empty_dataframe() -> pd.DataFrame:

    df = pd.DataFrame(columns=['msg1_index', 'msg2_index', 'related', 'manual_filled'])

    for idx in range(len(random_indexes)):
        first_idx_list = [random_indexes[idx]] * len(random_indexes)
        nones_list = [None] * len(random_indexes)
        manual_fills = [0] * len(random_indexes)
        # print(first_idx_list)
        # print(random_indexes)
        # print(nones_list)

        new_df = pd.DataFrame({
            'msg1_index': first_idx_list, 
            'msg2_index': random_indexes,
            'related': nones_list,
            'manual_filled': manual_fills
        })

        df = pd.concat([df, new_df])
        df.reset_index(inplace=True, drop=True)
    
    return df



def is_misinformation(value: int) -> bool:
    return "SIM" if value == 1 else "NÃO"


def get_num_marks() -> int:
    return os.get_terminal_size(0)[0]


def run() -> None:
    # TODO: Salvar dado em arquivo para cada iteração
    # via 'append'
    # A ideia é recuperar um arquivo já salvo e ir incrementando
    # até que seja possível continuar de onde foi parado
    if os.path.exists('sim_gt.csv'):
        data = pd.read_csv('sim_gt.csv')
        # last_idx = random_indexes.index(data.iloc[-1]['msg1_index'])
        # last_idy = random_indexes.index(data.iloc[-1]['msg2_index'])
        # os.system('clear')
        # print(data.head(10))

    else:
        data = create_empty_dataframe()

    existent = data['related'].notnull().sum()
    print(f"Found existing pre-saved data with {existent} filled values. Starting input collection...")
    time.sleep(5)


    counter = 0
    num_input_fills = int(data['manual_filled'].sum())

    os.system('clear')

    for idx in random_indexes:
        for idy in random_indexes:
            # print(data.iloc[counter])
            # import ipdb; ipdb.set_trace()
            if pd.isnull(data.iloc[counter]['related']):

                if idx == idy:
                    print("Same index - IGNORE!!! Assigning 1.")
                    print(get_num_marks() * "#")
                    data.at[counter, 'related'] = int(1)
                    # time.sleep(0.05)
                    os.system('clear')

                else:
                    # If the inverted index does not exist yet
                    if pd.isnull( data.loc[(data['msg1_index'] == idy) & (data['msg2_index'] == idx)]['related']  ).iloc[0]:

                        print(f"[{(counter):5}/{MAX_QUESTIONS} | {round(100*((counter)/MAX_QUESTIONS), 2):.2f} %completed] (Total entries filled)")
                        print(f"[{num_input_fills:5}/{REAL_FILLS_MAX:5} | {round(100*((num_input_fills)/REAL_FILLS_MAX), 2):.2f} %completed] (Real entries filled)")

                        print(get_num_marks() * "#")
                        print(f"MENSAGEM {idx:04d}  Desinformação: {is_misinformation(df.iloc[idx]['misinformation'])}")
                        print(get_num_marks() * "-")
                        print(df.iloc[idx]['text'])

                        print("\n")
                        print(get_num_marks() * "#")
                        # print("\n")
                        
                        print(f"MENSAGEM {idy:04d}  Desinformação: {is_misinformation(df.iloc[idy]['misinformation'])}")
                        print(get_num_marks() * "-")
                        print(df.iloc[idy]['text'])

                        
                        try:
                            are_related = input("\nAre they related to each other? ")
                            # are_related = np.random.choice(['0', '1'])
                            num_input_fills += 1

                        except KeyboardInterrupt as key_err:
                            print("\n\nOK. File has been saved as is.")
                            data.to_csv('sim_gt.csv', index=False)
                            print("Leaving script...\n")
                            sys.exit(0)

                        print(f"\nSelected: {are_related}")

                        if are_related not in ['0', '1']:
                            print("\nNo valid input provided. File will be saved as is.")
                            data.to_csv('sim_gt.csv', index=False)
                            print("Leaving script...\n")
                            sys.exit(0)

                        # temp_data = pd.DataFrame({
                        #     'msg1_index': [idx],
                        #     'msg2_index': [idy],
                        #     'related':    [int(are_related)],
                        # })

                        # data = pd.concat([data, temp_data])
                        # data.reset_index(inplace=True, drop=True)
                        # data.iloc[counter]['related'] = int(are_related)
                        data.at[counter, 'related'] = int(are_related)
                        data.at[counter, 'manual_filled'] = 1

                        # print(data.head(20))
                        time.sleep(0.75)
                        os.system('clear')
                    
                    # If the inverted index already exists
                    else:
                        print("The inverted index already exists - IGNORE!!! Assigning already existent value.")
                        print(get_num_marks() * "#")
                        print(f"Current index: {idx}|{idy}")
                        print(f"Existent index: \n{data.loc[(data['msg1_index'] == idy) & (data['msg2_index'] == idx)]}")
                        data.at[counter, 'related'] = int(data.loc[(data['msg1_index'] == idy) & (data['msg2_index'] == idx)]['related'].iloc[0])
                        # time.sleep(0.05)
                        os.system('clear')

            counter += 1

        # If the end of the file has not been reached yet
        if counter < data.shape[0]:
            # When reaching the end of the row, display a timed
            # alert if the first value of the next row is not yet filled
            if pd.isnull(data.iloc[counter+1]['related']):
                for i in range(5, 0, -1):
                    print(f"Changing target message in {i}s...")
                    time.sleep(1)
                    os.system('clear')
    
    data.to_csv('sim_gt.csv', index=False)
            
    return data
            

if __name__ == '__main__':
    os.system('clear')
    final_df = run()
    print(final_df)
