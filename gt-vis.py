# coding: utf-8
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns

sns.set_theme()

df = pd.read_csv('sim_gt.csv')

xlabels = np.unique(df['msg1_index'].to_numpy())
ylabels = np.unique(df['msg2_index'].to_numpy())


def display_autofilled():
    m_filled = df['manual_filled']
    m_filled_ = m_filled.to_numpy().reshape((100, 100))

    _ = sns.heatmap(
        m_filled_,
        linewidths=.5,
        cmap="Spectral",
        cbar=False,
        xticklabels=xlabels,
        yticklabels=ylabels
    )

    plt.title("Automatically filled entries")
    plt.show()

def display_totalfilled():
    related = df['related'].to_numpy()
    related_ = related.reshape((100, 100))

    _ = sns.heatmap(
        related_,
        linewidths=.5,cmap="Spectral",
        cbar=False,
        xticklabels=xlabels,
        yticklabels=ylabels
    )

    plt.title("Filled entries")
    plt.show()



if __name__ == '__main__':
    # display_autofilled()
    display_totalfilled()
